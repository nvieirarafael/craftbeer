package com.beerhouse.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.OptionalDouble;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.beerhouse.model.Beer;
import com.beerhouse.model.BeerRating;
import com.beerhouse.model.Category;
import com.beerhouse.model.Ingredient;
import com.beerhouse.repository.BeerRatingRepository;
import com.beerhouse.repository.BeerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class BeerRatingServiceIntegrationTest {
	
	@Autowired
	private BeerRatingService beerRatingService;
	@Autowired
	private BeerRatingRepository beerRatingRepository;	
	@Autowired
	private BeerRepository beerRepository;
	
	private Beer beer;
	private List<BeerRating> createdBeerRatings;
	
	@Before
	public void setup() {
		createdBeerRatings = new ArrayList<>();
		beer = beerRepository.save(new Beer("Stella Artois", new HashSet<Ingredient>(), "17%", BigDecimal.valueOf(19.90), Category.Ale));		
	}
	
	@Test
	public void testCreateBeerRating() {				
		BeerRating newBeerRating = beerRatingService.create(beer.getId(), 5, "It's a great Beer");
		createdBeerRatings.add(newBeerRating);
				
		// Verify the addition
		assertNotNull(newBeerRating);
		assertNotNull(newBeerRating.getId());
		assertEquals("It's a great Beer", newBeerRating.getComment());		
	}
	
	@Test
	public void testGetAllRatingsForBeer() {
		List<Beer> beers = beerRepository.findByCategory(Category.Ale);
				
		List<BeerRating> beerRatings = beerRatingService.getAllRatingsForBeer(beers.get(0).getId());	
		
		assertNotNull(beerRatings);
		assertTrue(beerRatings.size() > 0);
	}
	
	@Test
	public void testGetAverage() {
		BeerRating newBeerRating = beerRatingService.create(beer.getId(), 2, "It's a normal Beer");
		createdBeerRatings.add(newBeerRating);
		newBeerRating = beerRatingService.create(beer.getId(), 5, "It's a great Beer");
		createdBeerRatings.add(newBeerRating);
		
		Double average = beerRatingService.getAverage(beer.getId());
		
		assertEquals(average, getCorrectAverage());
	}
	
	@Test
	public void testDelete() {
		BeerRating newBeerRating = beerRatingService.create(beer.getId(), 2, "It's a normal Beer");		
		
		beerRatingService.delete(beer.getId(), newBeerRating.getId());
		
		BeerRating foundedBeer = beerRatingRepository.findByBeerAndId(beer, newBeerRating.getId());
		
		assertNull(foundedBeer);
	}
	
	private Double getCorrectAverage() {
		OptionalDouble correctAverage = createdBeerRatings.stream().mapToInt(BeerRating::getScore).average();
		return correctAverage.isPresent() ? correctAverage.getAsDouble() : null;		
	}
}
