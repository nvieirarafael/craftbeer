package com.beerhouse.controller;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.beerhouse.dto.RatingDto;
import com.beerhouse.model.Beer;
import com.beerhouse.model.BeerRating;
import com.beerhouse.model.Category;
import com.beerhouse.model.Ingredient;
import com.beerhouse.repository.BeerRatingRepository;
import com.beerhouse.repository.BeerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BeerRatingControllerIntegrationTest {

	@Autowired
	BeerRatingController beerRatingController;
	
	@Autowired
	private BeerRatingRepository beerRatingRepository;	
	@Autowired
	private BeerRepository beerRepository;

	private Beer beer;	
	
	@Before
	public void setup() {
		beer = beerRepository.save(new Beer("Stella Artois", new HashSet<Ingredient>(), "17%", BigDecimal.valueOf(19.90), Category.Ale));		
	}
	
	@Test
	public void testPostBeerRating() {		
		RatingDto ratingDto = new RatingDto(5, "Its a great beer");		
		
		beerRatingController.createBeerRating(beer.getId(), ratingDto);
		
		List<BeerRating> beerRatings = beerRatingRepository.findByBeer(beer);
				
		assertTrue(beerRatings.size() > 0);
	}
	
	@Test
	public void testGetAllRatingsForBeer() {
		beerRatingRepository.save(new BeerRating(5, "Its a great beer", beer));
		
		List<BeerRating> beerRatings = beerRatingController.getAllRatingsForBeer(beer.getId());
				
		assertTrue(! beerRatings.isEmpty());		
	}
	
	@Test
	public void testGetAverage() {
		beerRatingRepository.save(new BeerRating(5, "Its a great beer", beer));
		beerRatingRepository.save(new BeerRating(0, "Its a horrible beer", beer));
		
		SimpleEntry<String, Double> result = beerRatingController.getAverage(beer.getId());		
		
		assertTrue(result.getValue().equals(2.5));		
	}
	
	@Test
	public void testDelete() {
		BeerRating beerRating = beerRatingRepository.save(new BeerRating(5, "Its a great beer", beer));
		
		beerRatingController.delete(beer.getId(), beerRating.getId());	
		
		BeerRating foundedBeer = beerRatingRepository.findByBeerAndId(beer, beerRating.getId());
		
		assertNull(foundedBeer);	
	}
}
