package com.beerhouse;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.beerhouse.controller.BeerRatingControllerIntegrationTest;
import com.beerhouse.service.BeerRatingServiceIntegrationTest;

@RunWith(Suite.class)
@Suite.SuiteClasses ({BeerRatingServiceIntegrationTest.class, BeerRatingControllerIntegrationTest.class })
public class BeerRatingTestSuite {
}
