package com.beerhouse.service;

import java.util.List;

import com.beerhouse.model.BeerRating;

public interface BeerRatingService {

	BeerRating create(int beerId, int score, String comment);

	List<BeerRating> getAllRatingsForBeer(int beerId);

	void delete(int beerId, int ratingId);

	Double getAverage(int beerId);

}