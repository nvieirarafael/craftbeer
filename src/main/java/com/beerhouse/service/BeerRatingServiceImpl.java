package com.beerhouse.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.OptionalDouble;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beerhouse.model.Beer;
import com.beerhouse.model.BeerRating;
import com.beerhouse.repository.BeerRatingRepository;
import com.beerhouse.repository.BeerRepository;

@Service
public class BeerRatingServiceImpl implements BeerRatingService {
    private BeerRepository beerRepository;
    private BeerRatingRepository beerRatingRepository;

    @Autowired
    public BeerRatingServiceImpl(BeerRepository beerRepository, BeerRatingRepository beerRatingRepository) {
        this.beerRepository = beerRepository;
        this.beerRatingRepository = beerRatingRepository;
    }
        
    @Override
	public BeerRating create(int beerId, int score, String comment) {
    		Beer beer = verifyBeer(beerId);
		return beerRatingRepository.save(new BeerRating(score, comment, beer));
	}
    
    @Override
	public List<BeerRating> getAllRatingsForBeer(int beerId) {
    		Beer beer = verifyBeer(beerId);
		return beerRatingRepository.findByBeer(beer);
	}
    
    @Override
	public void delete(int beerId, int ratingId) {
    		BeerRating beerRating = verifyBeerRating(beerId, ratingId);
		beerRatingRepository.delete(beerRating);
	}
    
    @Override
	public Double getAverage(int beerId) {
		Beer beer = verifyBeer(beerId);
		
		List<BeerRating> ratings = beerRatingRepository.findByBeer(beer);
		OptionalDouble average = ratings.stream().mapToInt(BeerRating::getScore).average();
		
		return average.isPresent() ? average.getAsDouble() : null;
	}
    
    private Beer verifyBeer(int beerId) throws NoSuchElementException {
		Beer beer = beerRepository.findOne(beerId);
		if (beer == null) {
			throw new NoSuchElementException("Beer does not exist " + beerId);
		}
		return beer;
	}
    
    private BeerRating verifyBeerRating(int beerId, int ratingId) throws NoSuchElementException {
		Beer beer = beerRepository.findOne(beerId);
		BeerRating beerRating = beerRatingRepository.findByBeerAndId(beer, ratingId);

		if (beerRating == null) {
			throw new NoSuchElementException("Beer-Rating pair for request(" + beerId + " for rating " + ratingId);
		}
		return beerRating;
	}
}

