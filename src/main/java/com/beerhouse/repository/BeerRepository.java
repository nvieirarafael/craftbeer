package com.beerhouse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.beerhouse.model.Beer;
import com.beerhouse.model.Category;

public interface BeerRepository extends CrudRepository<Beer, Integer> {
    List<Beer> findByCategory(@Param("category") Category category);
}

