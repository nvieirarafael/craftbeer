package com.beerhouse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.beerhouse.model.Beer;
import com.beerhouse.model.BeerRating;

@RepositoryRestResource(exported = false)
public interface BeerRatingRepository extends CrudRepository<BeerRating, Integer> {
	List<BeerRating> findByBeer(Beer beer);
	BeerRating findByBeerAndId(Beer beer, Integer ratingId);
}