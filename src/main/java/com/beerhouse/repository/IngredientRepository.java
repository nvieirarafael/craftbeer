package com.beerhouse.repository;

import org.springframework.data.repository.CrudRepository;

import com.beerhouse.model.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Integer> {
}

