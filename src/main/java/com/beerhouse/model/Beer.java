package com.beerhouse.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="beer")
public class Beer implements Serializable {
	private static final long serialVersionUID = 3135004840646650153L;
	
	@Id	
    @GeneratedValue
    private Integer id;
	
	@NotEmpty
	@Column(name="name")
    private String name;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
    		name = "beer_ingredient", 
    		joinColumns = @JoinColumn(name = "beer_id", referencedColumnName = "id"), 
    		inverseJoinColumns = @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    	)    
    private Set<Ingredient> ingredients;
	
	@NotEmpty
	@Column(name="alcohol_content")
	private String alcoholContent;
	
	@NotNull
	@Column
	private BigDecimal price;
	
	@NotNull
	@Column
	private Category category;	
	
	public Beer(String name, Set<Ingredient> ingredients, String alcoholContent, BigDecimal price,
			Category category) {
		super();
		this.name = name;
		this.ingredients = ingredients;
		this.alcoholContent = alcoholContent;
		this.price = price;
		this.category = category;
	}
	
	protected Beer() {}
	
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public String getAlcoholContent() {
		return alcoholContent;
	}

	public void setAlcoholContent(String alcoholContent) {
		this.alcoholContent = alcoholContent;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}		
}
