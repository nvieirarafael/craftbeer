package com.beerhouse.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="ingredient")
public class Ingredient implements Serializable {	
	private static final long serialVersionUID = -8185959576196467834L;

	@Id	
    @GeneratedValue
    private Integer id;
	
	@NotEmpty
	@Column
    private String name;
	
	@ManyToMany(mappedBy = "ingredients")
	private Set<Beer> beers;

	protected Ingredient() {}
	
	public Integer getId() {
		return id;
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Beer> getBeers() {
		return beers;
	}

	public void setBeers(Set<Beer> beers) {
		this.beers = beers;
	}
}
