package com.beerhouse.model;

public enum Category {
	Ale, Lambic, Lager, Hybrid;
}
