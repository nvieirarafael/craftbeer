package com.beerhouse.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="beer_rating")
public class BeerRating implements Serializable {
	private static final long serialVersionUID = 3135004840646650153L;
	
	@Id	
    @GeneratedValue
    private Integer id;
	
    @Column(nullable = false)
    private Integer score;
    
    @Column
    private String comment;
    
    @ManyToOne
    @JoinColumn(name="beer_id")
    private Beer beer;    

    public BeerRating(Integer score, String comment, Beer beer) {
        this.score = score;
        this.comment = comment;
        this.beer = beer;
    }

    protected BeerRating() {
    }

    public Integer getId() {
		return id;
	}

	public Integer getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

	public Beer getBeer() {
		return beer;
	}

	public void setBeer(Beer beer) {
		this.beer = beer;
	}
}
