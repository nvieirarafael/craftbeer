package com.beerhouse.controller;

import java.util.AbstractMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.beerhouse.dto.RatingDto;
import com.beerhouse.model.BeerRating;
import com.beerhouse.repository.BeerRatingRepository;
import com.beerhouse.repository.BeerRepository;
import com.beerhouse.service.BeerRatingService;

@RestController
@RequestMapping(path = "/beers/{beerId}/ratings")
public class BeerRatingController {

	@Autowired
	BeerRatingService beerRatingService;

	BeerRepository beerRepository;
	BeerRatingRepository beerRatingRepository;

	@Autowired
	public BeerRatingController(BeerRepository beerRepository, BeerRatingRepository beerRatingRepository) {
		this.beerRepository = beerRepository;
		this.beerRatingRepository = beerRatingRepository;
	}

	protected BeerRatingController() {}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void createBeerRating(@PathVariable(value = "beerId") int beerId,
			@RequestBody @Validated RatingDto ratingDto) {
		beerRatingService.create(beerId, ratingDto.getScore(), ratingDto.getComment());
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<BeerRating> getAllRatingsForBeer(@PathVariable(value = "beerId") int beerId) {
		return beerRatingService.getAllRatingsForBeer(beerId);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/average")
	public AbstractMap.SimpleEntry<String, Double> getAverage(@PathVariable(value = "beerId") int beerId) {
		Double average = beerRatingService.getAverage(beerId);		
		return new AbstractMap.SimpleEntry<String, Double>("average", average);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{ratingId}")
	public void delete(@PathVariable(value = "beerId") int beerId, @PathVariable(value = "ratingId") int ratingId) {
		beerRatingService.delete(beerId, ratingId);
	}	
}
